# Contributor: Andy Postnikov <apostnikov@gmail.com>
# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Contributor: Fabio Ribeiro <fabiorphp@gmail.com>
# Maintainer: Andy Postnikov <apostnikov@gmail.com>
pkgname=php82-pecl-xdebug
_extname=xdebug
pkgver=3.2.0_alpha1
_pkgver=${pkgver/_/}
pkgrel=0
pkgdesc="PHP 8.2 extension that provides functions for function traces and profiling - PECL"
url="https://pecl.php.net/package/xdebug"
arch="all"
license="PHP-3.0"
depends="php82-common"
makedepends="php82-dev linux-headers"
source="php-pecl-$_extname-$_pkgver.tgz::https://pecl.php.net/get/$_extname-$_pkgver.tgz
	php-header.h"
builddir="$srcdir/$_extname-$_pkgver"
provides="php82-xdebug=$pkgver-r$pkgrel" # for backward compatibility
replaces="php82-xdebug" # for backward compatibility

build() {
	cp "$srcdir"/php-header.h "$builddir"/src/lib/
	phpize82
	./configure --prefix=/usr --with-php-config=php-config82
	make
}

check() {
	# PECL package has no test suite.
	php82 -d zend_extension="$builddir"/modules/xdebug.so -r 'xdebug_info();'
}

package() {
	make INSTALL_ROOT="$pkgdir" install

	local _confdir="$pkgdir"/etc/php82/conf.d
	mkdir -p $_confdir
	# should go after opcache
	cat > $_confdir/50_$_extname.ini <<-EOF
		; Uncomment and configure mode https://xdebug.org/docs/all_settings#xdebug.mode
		;zend_extension=$_extname.so
		;xdebug.mode=off
	EOF

	install -D -m644 -t "$pkgdir"/usr/share/php82/xdebug/ contrib/tracefile-analyser.php
	install -D -m644 -t "$pkgdir"/usr/share/vim/vimfiles/syntax/ contrib/xt.vim
}

sha512sums="
a46a57cfa566237786a2770e1cdd38443355ad1a5c370136e7a28dda2d306aa8566ee824c7135b27621a071563a88b0f8c2d1cebcc88bbdc349e3e1d15b14e6b  php-pecl-xdebug-3.2.0alpha1.tgz
fbeab5cbbfca69e069c2a86662ce9b522733fadaf40b3b45b1226606f97610b6f1e8451dfbf3284c568e20e43d75110eed3ec83544d7ece1059d9242c482b726  php-header.h
"
