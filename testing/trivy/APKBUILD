# Contributor: TBK <alpine@jjtc.eu>
# Maintainer: TBK <alpine@jjtc.eu>
pkgname=trivy
pkgver=0.30.0
pkgrel=1
pkgdesc="Simple and comprehensive vulnerability scanner for containers"
url="https://github.com/aquasecurity/trivy"
arch="all"
# s390x: tests SIGSEGV: https://github.com/aquasecurity/trivy/issues/430
# ppc64le: FTBFS: build constraints exclude all Go files in [...]
# riscv64: modernc.org/libc@v1.14.1 build constraints exclude all Go files
arch="$arch !s390x !ppc64le !riscv64"
license="Apache-2.0"
makedepends="btrfs-progs-dev go linux-headers lvm2-dev"
source="https://github.com/aquasecurity/trivy/archive/v$pkgver/trivy-$pkgver.tar.gz"
options="net !check" # needs tinygo to turn go into wasm for tests

build() {
	make build VERSION=$pkgver
}

check() {
	make test
}

package() {
	install -Dm755 $pkgname "$pkgdir"/usr/bin/$pkgname
}

sha512sums="
e36c4c0346ce398b1e589b30f41e4e77cb2ba12fbae53b80bd7e7d99c4b6d4c051df24546f04668bb369215ff59cfed66f32f7aa9faa9f80e9e6d00a964d66cf  trivy-0.30.0.tar.gz
"
